PARAMS = {
  FORM_OPEN_DATE  : "",
  FORM_CLOSE_DATE : "",
  SHEETID         : "",
  FORMID          : "",
  QUESTION_PREFIX : "Ik wil graag meedoen aan deze ",
  QUESTIONS       : ["training"],
  QUESTION_TYPE   : FormApp.ItemType.MULTIPLE_CHOICE
}
CONSTS = {
  VOLGEBOEKT : "Inschrijven is niet meer mogelijk. Voor volgende week kun je je weer aanmelden vanaf zaterdag 16:00."
}

  
/* Initialize the form, setup time based triggers */
function Initialize() {
  deleteTriggers_(); 
  checkLimit(); 
  
  if (PARAMS.FORM_OPEN_DATE !== "") {
    ScriptApp.newTrigger("openForm").timeBased().at(parseDate_(PARAMS.FORM_OPEN_DATE)).create();
  }

  if (PARAMS.FORM_CLOSE_DATE !== "") {
    ScriptApp.newTrigger("closeForm").timeBased().at(parseDate_(PARAMS.FORM_CLOSE_DATE)).create();
  }
  
  ScriptApp.newTrigger("checkLimit").forForm(FormApp.openById(PARAMS.FORMID)).onFormSubmit().create();
}

/* If Total # of Form Responses >= Limit, Close Form */
function checkLimit() {
  var form = FormApp.openById(PARAMS.FORMID)
  var spreadsheet= SpreadsheetApp.openById(PARAMS.SHEETID)

  var questions = form.getItems() //(FormApp.ItemType.CHECKBOX)
  var keepOpen = false //tot we een invulbare vraag tegenkomen.
   
  //sheet openen. Kijken welke sheets we hebben. Per sheet de vraag ophalen
  if(spreadsheet !== null) { 
    for(const sheetname of PARAMS.QUESTIONS) {
      var sheet = spreadsheet.getSheetByName(sheetname)
      if (sheet === null) { 
        informUser_("Can't find sheetname: " + sheetname);
        continue; 
      } else {
      
        var question = null
        for(let i in questions) {
          if (questions[i].getTitle() === PARAMS.QUESTION_PREFIX + sheetname + ":") {
            question = questions[i]; 
            break;
          }
        }
        if (question === null) {
          switch(PARAMS.QUESTION_TYPE) {
            case FormApp.ItemType.CHECKBOX:
              question = form.addCheckboxItem()
              break;
            case FormApp.ItemType.MULTIPLE_CHOICE:
              question = form.addMultipleChoiceItem()
              break;
          }//end of switch(question_type)
          question
          .setTitle(PARAMS.QUESTION_PREFIX + sheetname + ":")
          .setRequired(PARAMS.QUESTIONS.length == 1) //als er 1 vraag is, is ie verplicht. bij meer vragen zijn ze allemaal NIET verplicht.
        }//end of if(question === null)
        
        var range = sheet.getRange(2,1, sheet.getLastRow(), 3)
        var choices = [] //possible choices
        var closed = [] //unavailable choices
        
        for (var r = 1; r < sheet.getLastRow(); r++){ //afwijking van 1 rij
          var text = ""
          var plekken = -1
          
          for (var c = 1; c <= 2; c++){ //3 hoeft niet.
            cell = range.getCell(r, c)
            switch (c){
              case 1: //Naam optie
                text = cell.getValue() + text; 
                break;
              case 2: //plekken over
                plekken = cell.getValue(); 
                break;
            }//end of switch(c)
          }//end of for(c)
          
          if (plekken > 0) { //optie over.
            choices.push(text + " (" + plekken + " plekken over)")
          } else { // optie vol. en nu?          
            closed.push(text)
            if (range.getCell(r, 3).getValue() == "") { //nog niet gemeld. even melden.
              range.getCell(r, 3).setValue("'" + new Date(Date.now()).toLocaleString('nl-NL')); // tijd van NU zetten.
              informUser_(form.getTitle() + " - Training: " + text + " is nu vol!") //en mailen
            }
          }// plekken > 0 ?       
        }//end of for(r)
        
        
        if (choices.length > 0) {            
          keepOpen = true;
          switch(PARAMS.QUESTION_TYPE) {
            case FormApp.ItemType.CHECKBOX:
              question.asCheckboxItem()
              .setChoiceValues(choices)
              .setHelpText("")
              .setRequired(PARAMS.QUESTIONS.length == 1) //als er 1 vraag is, is ie verplicht. bij meer vragen zijn ze allemaal NIET verplicht.
              break;
            case FormApp.ItemType.MULTIPLE_CHOICE:
              question.asMultipleChoiceItem()
              .setChoiceValues(choices)
              .setHelpText("")
              .setRequired(PARAMS.QUESTIONS.length == 1) //als er 1 vraag is, is ie verplicht. bij meer vragen zijn ze allemaal NIET verplicht.
              break;
          }
        } else {
          switch(PARAMS.QUESTION_TYPE) {
            case FormApp.ItemType.CHECKBOX:
              question.asCheckboxItem()
              .setChoiceValues(["Alle trainingen zijn helaas volgeboekt."])
              .setRequired(false); 
              break;
            case FormApp.ItemType.MULTIPLE_CHOICE:
              question.asMultipleChoiceItem()
              .setChoiceValues(["Alle trainingen zijn helaas volgeboekt."])
              .setRequired(false); 
              break;
          }                
        } //setting values for question      
        
        if(closed.length > 0) {
          switch(PARAMS.QUESTION_TYPE) {
            case FormApp.ItemType.CHECKBOX:
              question.asCheckboxItem()
              .setHelpText("Deze training" + (closed.length == 1 ? " is " : "en zijn ") + "volgeboekt: " + closed.join("; ") + ". " + CONSTS.VOLGEBOEKT ); 
              break;
            case FormApp.ItemType.MULTIPLE_CHOICE:
              question.asMultipleChoiceItem()
              .setHelpText("Deze training" + (closed.length == 1 ? " is " : "en zijn ") + "volgeboekt: " + closed.join("; ") + ". " + CONSTS.VOLGEBOEKT ); 
              break;
          }                
        } //Gesloten trainingen tonen
      }//sheet !== null
    }//end of for(sheetname)    
  }//spreadsheet !== null

  if(! keepOpen) { 
    closeForm("Alle trainingen zijn vol. " + CONSTS.VOLGEBOEKT)
  }

}

/* Delete all existing Script Triggers */
function deleteTriggers_() {
  var triggers = ScriptApp.getProjectTriggers();
  for (var i in triggers) {
    ScriptApp.deleteTrigger(triggers[i]);
  }
}

/* Send a mail to the form owner when the form status changes */
function informUser_(subject) {
    MailApp.sendEmail(Session.getActiveUser().getEmail(), subject, FormApp.openById(PARAMS.FORMID).getPublishedUrl()); //body = view url. Simpel maar doeltreffend?
}

/* Open the Google Form */
function openForm() {
  var form = FormApp.openById(PARAMS.FORMID)
  form.setAcceptingResponses(true);
  informUser_(form.getTitle() + " Het formulier is geopend.");
}

/* Close the Google Form, Stop Accepting Reponses */
function closeForm(closedmessage = CONSTS.VOLGEBOEKT) {
  var form = FormApp.openById(PARAMS.FORMID)
  form.setAcceptingResponses(false).setCustomClosedFormMessage(closedmessage);
  deleteTriggers_();
  informUser_(form.getTitle() + "SHEET - Alle trainingen zijn vol. Het formulier is gesloten.");
}

/* Parse the Date for creating Time-Based Triggers */
function parseDate_(d) {
  return new Date(d.substr(0, 4), d.substr(5, 2) - 1, d.substr(8, 2), d.substr(11, 2), d.substr(14, 2));
}
